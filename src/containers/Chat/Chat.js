import React, {useEffect, useRef, useState} from 'react';
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {getUser} from "../../store/actions";

const Chat = (props) => {
    const user = useSelector(state => state.user);
    const dispatch = useDispatch()

    const [message, setMessage] = useState("");
    const [messages, setMessages] = useState([])


    const logOut = async () => {
        const headers = {"Authorization": user.token};
        await axios.delete('http://localhost:8000/users/sessions', {headers});
        props.history.push("/")
        dispatch(getUser(null));
    };

    const addMessage = event => {
        setMessage(event.target.value);
    };

    const ws = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket("ws://localhost:8000/chat");

        ws.current.onopen = () => {
            ws.current.send(JSON.stringify({type: "GET_ALL_MESSAGES"}));
            ws.current.send(JSON.stringify({type: "CONNECT_USER", user: user}));
        };

        ws.current.onclose = () => console.log("ws connection closed");
        ws.current.onmessage = e => {
            const decodedMessage = JSON.parse(e.data);
            console.log(decodedMessage)
            if (decodedMessage.type === "NEW_MESSAGE") {
                setMessages(messages => [...messages, decodedMessage.message]);
            } else if (decodedMessage.type === "ALL_MESSAGES") {
                setMessages(messages => [...messages, ...decodedMessage.messages]);
            }
        };
        return () => ws.current.close();
    }, []);

    const sendMessage = e => {
        e.preventDefault();
        ws.current.send(JSON.stringify({
            type: "CREATE_MESSAGE",
            text: message
        }));
    };

    if (user) {
        return (
            <div className="container">
                <header>
                    <h2>Chat</h2>
                    <span className="displayName" onClick={logOut}>{user.displayName}</span>
                </header>
                <div className="content">
                    <div className="online">
                        <h3>Online users</h3>
                    </div>
                    <div className="chat">
                        <div className="messages">
                            {
                                messages.map((message, idx) => {
                                    return <div key={idx}>
                                        <b>{message.displayName}: </b>
                                        {message.text}
                                    </div>
                                })
                            }
                        </div>
                        <div className="sendBlock">
                            <textarea
                                className="text" placeholder="Write your message."
                                value={message.message} onChange={addMessage}
                            />
                            <button className="sendBtn" onClick={sendMessage}>Send</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return null
    }
};

export default Chat;