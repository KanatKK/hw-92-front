import React, {useState} from 'react';
import axios from "axios";
import {useDispatch} from "react-redux";
import {getUser} from "../../store/actions";

const Login = (props) => {
    const dispatch = useDispatch();

    const [registration, setRegistration] = useState({
        username: "",
        displayName: "",
        password: "",
    });
    const [errors, setErrors] = useState(null);

    const userDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setRegistration(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const toSignUp = () => {
        props.history.push("/")
        setRegistration({
            username: "",
            displayName: "",
            password: "",
        });
        setErrors(null);
    }

    const login = async event => {
        event.preventDefault();
        try {
            const response = await axios.post('http://localhost:8000/users/sessions', registration);
            await dispatch(getUser(response.data));
            props.history.push("/chat")
        } catch (e) {
            if (e.response && e.response.data) {
                setErrors(e.response.data);
            }
        }
    };

    return (
        <div className="container">
            <header><h2>Login</h2></header>
            <input
                type="text" placeholder="Username"
                className="regFields" value={registration.username}
                name="username" onChange={userDataChanger}
            />
            <input
                type="text" placeholder="Password"
                className="regFields" value={registration.password}
                name="password" onChange={userDataChanger}
            />
            <button className="addBtn" onClick={login}>Login</button>
            {errors && <span className="error">{errors.error}</span>}
            <p onClick={toSignUp} className="changer">Create Account</p>
        </div>
    );
};

export default Login;