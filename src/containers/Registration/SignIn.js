import React, {useState} from 'react';
import axios from "axios";
import {useDispatch} from "react-redux";
import {getUser} from "../../store/actions";

const SignIn = (props) => {
    const dispatch = useDispatch();
    const [registration, setRegistration] = useState({
        username: "",
        displayName: "",
        password: "",
    });
    const [errors, setErrors] = useState(null);

    const userDataChanger = event => {
        const name = event.target.name;
        const value = event.target.value;

        setRegistration(prevState => ({
            ...prevState,
            [name]: value
        }))
    };

    const createAccount = async event => {
        event.preventDefault();
        try {
            const response = await axios.post('http://localhost:8000/users', registration);
            dispatch(getUser(response.data));
            props.history.push("/chat")
        } catch (e) {
            if (e.response && e.response.data) {
                setErrors(e.response.data);
            }
        }
    };

    const toLogin = () => {
        props.history.push("/login")
        setRegistration({
            username: "",
            displayName: "",
            password: "",
        });
        setErrors(null);
    }

    return (
        <div className="container">
            <header><h2>Create Account</h2></header>
            <input
                type="text" placeholder="Add Username"
                className="regFields" value={registration.username}
                name="username" onChange={userDataChanger}
            />
            <input
                type="text" placeholder="Add Display Name"
                className="regFields" value={registration.displayName}
                name="displayName" onChange={userDataChanger}
            />
            <input
                type="text" placeholder="Create Password"
                className="regFields" value={registration.password}
                name="password" onChange={userDataChanger}
            />
            <button className="addBtn" onClick={createAccount}>Create</button>
            {errors && <span className="error">{errors.error}</span>}
            <p onClick={toLogin} className="changer">Already have an account? Login!</p>
        </div>
    );
};

export default SignIn;