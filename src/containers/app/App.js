import React from "react";
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import SignIn from "../Registration/SignIn";
import Login from "../Registration/Login";
import Chat from "../Chat/Chat";

const App = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={SignIn}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/chat" exact component={Chat}/>
            </Switch>
        </BrowserRouter>
    )
};

export default App;

