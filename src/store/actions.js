import {GET_USER} from "./actionTypes";

export const getUser = value => {
    return {type: GET_USER, value};
};